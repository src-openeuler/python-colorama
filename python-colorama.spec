%global _empty_manifest_terminate_build 0
Name:           python-colorama
Version:        0.4.6
Release:        1
Summary:        Cross-platform colored terminal text.
License:        BSD
URL:            https://github.com/tartley/colorama
Source0:        https://files.pythonhosted.org/packages/d8/53/6f443c9a4a8358a93a6792e2acffb9d9d5cb0a5cfd8802644b7b1c9a02e4/colorama-0.4.6.tar.gz
BuildArch:      noarch
%description
Makes ANSI escape character sequences (for producing colored terminal
text and cursor positioning) work under MS Windows.

%package -n python3-colorama
Summary:        Cross-platform colored terminal text.
Provides:       python-colorama
Obsoletes:      python-colorama-help < 0.4.6
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel python3-hatchling python3-hatch-vcs python3dist(pytest)

%description -n python3-colorama
Makes ANSI escape character sequences (for producing colored terminal
text and cursor positioning) work under MS Windows.

%prep
%autosetup -n colorama-0.4.6

%build
%pyproject_build

%install
%pyproject_install colorama==%{version}
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi

%check
pytest

%files -n python3-colorama
%{python3_sitelib}/*

%changelog
* Tue Apr 25 2023 wangkai <13474090681@163.com> - 0.4.6-1
- Update to 0.4.6
- Obsoletes python-colorama-help package

* Wed Sep 14 2022 Qiao Jijun <qiaojijun@kylinos.cn> - 0.4.5-1
- Update to 0.4.5

* Tue Jul 13 2021 OpenStack_SIG <openstack@openeuler.org> - 0.4.4-1
- Upgrade to version 0.4.4

* Wed Sep 30 2020 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
